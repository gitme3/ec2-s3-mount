resource "aws_s3_bucket" "s3_demo" {
  bucket = "my-demo-test-bucket8"
  acl    = "public-read"

  
  tags = {
     Name = "dev-bucket8"
     Environment = "dev"
  }
}