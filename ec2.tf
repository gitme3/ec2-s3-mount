resource "aws_instance" "ec2-demo" {
    ami               = "ami-065939d4b3a061186"
    instance_type     = "t2.micro"
    security_groups   = ["Ohio_ALL_traffic"]
    iam_instance_profile = aws_iam_instance_profile.ec2_s3_profile.name
    key_name          = "ohio"
    
    provisioner "remote-exec" {
        inline = [
            "sudo yum install automake fuse fuse-devel gcc-c++ git libcurl-devel libxml2-devel  make openssl-devel mailcap -y",
            "git clone https://github.com/s3fs-fuse/s3fs-fuse.git",
            "cd s3fs-fuse",
            "sudo ./autogen.sh",
            "sudo ./configure --prefix=/usr --with-openssl",
            "sudo make",
            "sudo make install",
            "sudo mkdir /s3mount",
            "sudo s3fs my-demo-test-bucket8 /s3mount -o iam_role=ec2_s3_profile"
        ]
    connection {
       type = "ssh"
       user = "centos"
       host = self.public_ip
       private_key = file("./ohio.pem")
      }
    }
  tags = {
       Name = "node-db"
    }
}


