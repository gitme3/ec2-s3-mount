resource "aws_eip" "ec2_eip" {
    vpc = true
}

output "eip" {
    value = aws_eip.ec2_eip.public_ip
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.ec2-demo.id
  allocation_id = aws_eip.ec2_eip.id
}